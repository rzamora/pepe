#pragma once

#include "info.h"

class ModuleSequence {

public:

	ModuleSequence(Info* info);
	~ModuleSequence(void);

	int Go();

private:

	Info * info;

	void FadeIn();
	void FadeOut();
	void ShowText(int x, int y, int color, int speed, const char* text1, const char* text2 = nullptr, const char* text3 = nullptr);
	void Wait(int time);
};
