#pragma once

#define ESTAT_EIXIR			-1
#define ESTAT_ICEKAS		0
#define ESTAT_LOGO			1
#define ESTAT_MENU			2
#define ESTAT_OPCIONS		3
#define ESTAT_SELECT		4
#define ESTAT_SEQUENCIA		5
#define ESTAT_JOC			6
#define ESTAT_LEVELCOMPLETE 7
#define ESTAT_MORT			8

#define SENSE_OPCIO			-1

#define MODE_FACIL			0
#define MODE_NORMAL			1
#define MODE_DIFICIL		2

#define PERSONATGE_PEPE		0
#define PERSONATGE_ROSITA	1
#define PERSONATGE_JOB		2

struct Info {
	int		estat_joc;
	bool	rosita_enabled;
	bool	job_enabled;
	int		dificultat;
	int		personatge;
};
