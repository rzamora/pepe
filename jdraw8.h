#pragma once
#include <SDL2/SDL.h>

struct Color {
	Uint8 r;
	Uint8 g;
	Uint8 b;
};

typedef Uint8* JD8_Surface;
typedef Color* JD8_Palette;

void JD8_Init(const char *title);

void JD8_Quit();

void JD8_ClearScreen(Uint8 color);

JD8_Surface JD8_NewSurface();

JD8_Surface JD8_LoadSurface(const char *file);

JD8_Palette JD8_LoadPalette(const char *file);

void JD8_SetScreenPalette(JD8_Palette palette);

void JD8_FillSquare(int ini, int height, Uint8 color);

void JD8_Blit(JD8_Surface surface);

void JD8_Blit(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh);

void JD8_BlitToSurface(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, JD8_Surface dest);

void JD8_BlitCK(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, Uint8 colorkey );

void JD8_BlitCKCut(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, Uint8 colorkey);

void JD8_BlitCKScroll(int y, JD8_Surface surface, int sx, int sy, int sh, Uint8 colorkey);

void JD8_BlitCKToSurface(int x, int y, JD8_Surface surface, int sx, int sy, int sw, int sh, JD8_Surface dest, Uint8 colorkey );

void JD8_Flip();

void JD8_FreeSurface(JD8_Surface surface);

Uint8 JD8_GetPixel( JD8_Surface surface, int x, int y );

void JD8_PutPixel( JD8_Surface surface, int x, int y, Uint8 pixel );
void JD8_PutPixel(int x, int y, Uint8 pixel);

void JD8_SetPaletteColor(Uint8 index, Uint8 r, Uint8 g, Uint8 b);
void JD8_SetPaletteColor(Uint8 index, Uint8 other);

void JD8_FadeOut();
bool JD8_FadeOutAsync();

void JD8_FadeToPal( JD8_Palette pal );
bool JD8_FadeToPalAsync(JD8_Palette pal);

//JD_Font JD_LoadFont( char *file, int width, int height);

//void JD_DrawText( int x, int y, JD_Font *source, char *text);

//char *JD_GetFPS();
