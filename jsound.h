#pragma once
#include <SDL2/SDL_mixer.h>

typedef Mix_Chunk JS_Sound;

bool JS_Init();

void JS_LoadMusic(const char *musicFilename);

void JS_SetMusicVolume(int volume);

void JS_PlayMusic(int loops);

void JS_PauseMusic();

void JS_FadeOutMusic();

bool JS_MusicPlaying();

const char* JS_GetMusicName();

JS_Sound *JS_LoadSound(char *soundFilename);

void JS_SetSoundVolume(JS_Sound *sound, int volume);

void JS_PlaySound(JS_Sound *sound);

void JS_FreeSound(JS_Sound *sound);

void JS_Finalize();
