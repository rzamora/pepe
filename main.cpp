#include "jgame.h"
#include "jdraw8.h"
#include "jsound.h"
#include "jfile.h"
#include "info.h"
#include "modulestaticscreen.h"
#include "modulemenu.h"
#include "moduleselect.h"
#include "modulesequence.h"
#include "time.h"
#include <string>

#ifndef WIN32
#include <libgen.h>
#endif

int main( int argc, char* args[] ) {

#ifdef WIN32
	JF_SetResourceFile("data.jrf");
#else
	char res_file[255] = "";
	strcpy(res_file, dirname(args[0]));
#ifdef __APPLE__
    strcat(res_file, "/../Resources/data.jrf");
#else
    strcat(res_file, "/data.jrf");
#endif
	printf("ARXIU DE RECURSOS: %s\n", res_file);
	JF_SetResourceFile(res_file);
#endif

	srand( unsigned(time(NULL)) );

	JG_Init();
	JD8_Init("Pepe El Pintor");
	JS_Init();

	Info info;
	info.estat_joc = ESTAT_ICEKAS; // ESTAT_SEQUENCIA; // ESTAT_ICEKAS;
	info.rosita_enabled = false;
	info.job_enabled = false;
	info.dificultat = MODE_FACIL;
	info.personatge = PERSONATGE_PEPE;

	FILE* ini = fopen("trick.ini", "rb");
	if (ini != NULL) {
		unsigned char contingut = 0;
		fread(&contingut, 1, 1, ini);
		fclose(ini);
		if (contingut > 0) info.rosita_enabled = true;
		if (contingut > 1) info.job_enabled = true;
	}

	while (info.estat_joc != ESTAT_EIXIR) {
		switch (info.estat_joc) {
			case ESTAT_ICEKAS:
			case ESTAT_LOGO:
				ModuleStaticScreen * moduleStaticScreen;
				moduleStaticScreen = new ModuleStaticScreen(&info);
				info.estat_joc = moduleStaticScreen->Go();
				delete moduleStaticScreen;
				break;
			case ESTAT_MENU:
				ModuleMenu * moduleMenu;
				moduleMenu = new ModuleMenu(&info);
				info.estat_joc = moduleMenu->Go();
				delete moduleMenu;
				break;
			case ESTAT_SELECT:
				ModuleSelect * moduleSelect;
				moduleSelect = new ModuleSelect(&info);
				info.estat_joc = moduleSelect->Go();
				delete moduleSelect;
				break;
			case ESTAT_SEQUENCIA:
				ModuleSequence * moduleSequence;
				moduleSequence = new ModuleSequence(&info);
				info.estat_joc = moduleSequence->Go();
				delete moduleSequence;
				break;
		}
	}

	JF_Quit();
	JS_Finalize();
	JD8_Quit();
	JG_Finalize();

	return 0;
}

