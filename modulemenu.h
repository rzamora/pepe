#pragma once

#include "info.h"

class ModuleMenu {

public:

	ModuleMenu(Info* info);
	~ModuleMenu(void);

	int Go();

private:

	//void showMenu();

	Info* info;
	int contador;
};
