#include "moduleselect.h"

#include "jgame.h"
#include "jdraw8.h"
#include "jinput.h"
#include "jsound.h"
#include <stdlib.h>
#include <string>

ModuleSelect::ModuleSelect(Info* info) {
	this->info = info;
}

ModuleSelect::~ModuleSelect(void) {
}

void pintaMarc(int posicio) {
	for (int i = posicio; i <= posicio + 75; i++) {
		JD8_PutPixel(i, 11, 15);
		JD8_PutPixel(i, 12, 15);
		JD8_PutPixel(i, 90, 15);
		JD8_PutPixel(i, 91, 15);
	}
	for (int i = 11; i <= 91; i++) {
		JD8_PutPixel(posicio, i, 15);
		JD8_PutPixel(posicio + 1, i, 15);
		JD8_PutPixel(posicio + 74, i, 15);
		JD8_PutPixel(posicio + 75, i, 15);
	}
}

int ModuleSelect::Go() {

	JD8_Surface fondo = JD8_LoadSurface("select.gif");
	JD8_Palette pal = JD8_LoadPalette("select.gif");
	JD8_Surface gfx = JD8_LoadSurface("sprites.gif");

	int num_personatges = 3;

	if (!this->info->rosita_enabled) {
		num_personatges--;
		for (int i = 137; i <= 212; i++) for (int j = 11; j <= 91; j++) JD8_PutPixel(fondo, i, j, 0);
		JD8_PutPixel(fondo, 145, 19, 14);
		JD8_PutPixel(fondo, 197, 44, 14);
		JD8_PutPixel(fondo, 149, 78, 14);
		JD8_PutPixel(fondo, 194, 77, 14);
		JD8_PutPixel(fondo, 172, 54, 14);
		JD8_PutPixel(fondo, 198, 65, 14);
		JD8_PutPixel(fondo, 177, 38, 9);
		JD8_PutPixel(fondo, 169, 82, 9);
		JD8_PutPixel(fondo, 183, 12, 9);
		JD8_PutPixel(fondo, 139, 37, 9);
		JD8_PutPixel(fondo, 211, 81, 9);
	}
	if (!this->info->job_enabled) {
		num_personatges--;
		for (int i = 219; i <= 294; i++) for (int j = 11; j <= 91; j++) JD8_PutPixel(fondo, i, j, 0);
		JD8_PutPixel(fondo, 228, 65, 14);
		JD8_PutPixel(fondo, 290, 26, 14);
		JD8_PutPixel(fondo, 264, 63, 14);
		JD8_PutPixel(fondo, 284, 85, 14);
		JD8_PutPixel(fondo, 262, 13, 14);
		JD8_PutPixel(fondo, 239, 46, 14);
		JD8_PutPixel(fondo, 266, 74, 9);
		JD8_PutPixel(fondo, 264, 21, 9);
		JD8_PutPixel(fondo, 248, 84, 9);
		JD8_PutPixel(fondo, 279, 87, 9);
		JD8_PutPixel(fondo, 254, 39, 9);
	}

	JD8_ClearScreen(0);
	JD8_Flip();

	// FADE IN
	JD8_Blit(fondo);
	JG_SetUpdateTicks(1);
	while (!JD8_FadeToPalAsync(pal) && !JG_Quitting()) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
	}

	int opcio_seleccionada = this->info->dificultat;
	int opcio_triada = SENSE_OPCIO;
	int personatge_seleccionat = this->info->personatge;
	bool fletxa_amunt_pulsada = false;
	bool fletxa_avall_pulsada = false;
	bool fletxa_esquerra_pulsada = false;
	bool fletxa_dreta_pulsada = false;

	JG_SetUpdateTicks(20);

	int i = 0;
	int G = 0;
	int B = 0;

	while (!JG_Quitting() && opcio_triada == SENSE_OPCIO) {
		while (!JG_ShouldUpdate()) {
			JI_Update();

			if (JI_KeyPressed(SDL_SCANCODE_UP)) {
				if (!fletxa_amunt_pulsada) {
					fletxa_amunt_pulsada = true;
					opcio_seleccionada--; if (opcio_seleccionada < 0) opcio_seleccionada = 2;
				}
			} else {
				fletxa_amunt_pulsada = false;
			}

			if (JI_KeyPressed(SDL_SCANCODE_DOWN)) {
				if (!fletxa_avall_pulsada) {
					fletxa_avall_pulsada = true;
					opcio_seleccionada++; if (opcio_seleccionada > 2) opcio_seleccionada = 0;
				}
			} else {
				fletxa_avall_pulsada = false;
			}

			if (JI_KeyPressed(SDL_SCANCODE_LEFT)) {
				if (!fletxa_esquerra_pulsada) {
					fletxa_esquerra_pulsada = true;
					personatge_seleccionat--; if (personatge_seleccionat < 0) personatge_seleccionat = num_personatges-1;
				}
			} else {
				fletxa_esquerra_pulsada = false;
			}

			if (JI_KeyPressed(SDL_SCANCODE_RIGHT)) {
				if (!fletxa_dreta_pulsada) {
					fletxa_dreta_pulsada = true;
					personatge_seleccionat++; if (personatge_seleccionat > num_personatges-1) personatge_seleccionat = 0;
				}
			} else {
				fletxa_dreta_pulsada = false;
			}

			if (JI_KeyPressed(SDL_SCANCODE_SPACE)) {
				opcio_triada = opcio_seleccionada;
			}
		}

		i = (i + 1) % 127;
		G = B = abs(63-i);
		JD8_SetPaletteColor(15, 252, G, B);

		JD8_Blit(fondo);
		pintaMarc(55 + personatge_seleccionat * 82);
		JD8_BlitCK(100, 120 + opcio_seleccionada * 25, gfx, 9, 5, 9, 10, 255);
		JD8_Flip();
	}
	JD8_BlitCKToSurface(100, 120 + opcio_seleccionada * 25, gfx, 9, 5, 9, 10, fondo, 255);

	// FADE OUT
	JG_SetUpdateTicks(2);
	while (!JD8_FadeOutAsync() && !JG_Quitting()) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
	}

	// FREE EVERYTHING
	JD8_FreeSurface(fondo);
	JD8_FreeSurface(gfx);
	free(pal);




	if (JG_Quitting()) {
		return ESTAT_EIXIR;
	} else {
		this->info->dificultat = opcio_triada;
		this->info->personatge = personatge_seleccionat;
		return ESTAT_SEQUENCIA;
	}
}
