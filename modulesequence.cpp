#include "modulesequence.h"

#include "jgame.h"
#include "jdraw8.h"
#include "jinput.h"
#include "jsound.h"
#include <stdlib.h>
#include <string>

JD8_Surface gfx = nullptr;
JD8_Surface fondo = nullptr;
JD8_Palette pal = nullptr;

ModuleSequence::ModuleSequence(Info* info) {
	this->info = info;
	gfx = nullptr;
	fondo = nullptr;
	pal = nullptr;
}

ModuleSequence::~ModuleSequence(void) {
}

int ModuleSequence::Go() {

	JS_LoadMusic("mhist.mid");
	JS_PlayMusic(-1);

	gfx = JD8_LoadSurface("sprites.gif");

	JD8_ClearScreen(0);
	JD8_Flip();

	fondo = JD8_LoadSurface("hist01.gif");
	pal = JD8_LoadPalette("hist01.gif");
	this->FadeIn();
	this->ShowText(40, 90, 2, 10, "QUE BON DIA FA NO CREUS PEPE");
	this->ShowText(50, 80, 10, 10, "EEEE..SS..SSIII.SI");
	this->ShowText(140, 140, 2, 10, "[SEMPRE QUE M^ACOSTE", "A PEPE ES POSA RARO\\");
	JD8_Blit(fondo);
	JD8_Flip();
	this->Wait(200);
	this->FadeOut();
	JD8_FreeSurface(fondo);
	free(pal);

	if (!JG_Quitting()) {
		fondo = JD8_LoadSurface("hist02.gif");
		pal = JD8_LoadPalette("hist02.gif");
		this->FadeIn();
		this->ShowText(100, 140, 10, 10, "AAAAY a");
		this->ShowText(100, 55, 3, 10, "JA T^HE PILLAT PEPE");
		this->ShowText(145, 125, 2, 1, "AUXILIa");
		JD8_Blit(fondo);
		JD8_Flip();
		this->Wait(200);
		this->FadeOut();
		JD8_FreeSurface(fondo);
		free(pal);
	}

	if (!JG_Quitting()) {
		fondo = JD8_LoadSurface("hist03.gif");
		pal = JD8_LoadPalette("hist03.gif");
		this->FadeIn();
		this->ShowText(50, 80, 5, 10, "ARA ROSITA ES MEUA");
		this->ShowText(50, 80, 5, 10, "SI VOLS ACONSEGUIR_LA", "HAURAS DE SEGUIR_ME", "PER TOT EL MONb");
		this->ShowText(60, 80, 5, 1, "JA JA JA JA JA aa");
		JD8_Blit(fondo);
		JD8_Flip();
		this->Wait(200);
		this->FadeOut();
		JD8_FreeSurface(fondo);
		free(pal);
	}

	if (!JG_Quitting()) {
		fondo = JD8_LoadSurface("hist04.gif");
		pal = JD8_LoadPalette("hist04.gif");
		this->FadeIn();
		this->ShowText(120, 60, 2, 5, "AJUDA^M PEPEa");
		this->ShowText(50, 50, 7, 50, "MALEIT SIGUES", "ET TROBAREbbb");
		this->FadeOut();
		JD8_FreeSurface(fondo);
		free(pal);
	}

	// FREE EVERYTHING
	JD8_FreeSurface(gfx);

	if (JG_Quitting()) {
		return ESTAT_EIXIR;
	} else {
		return ESTAT_JOC;
	}
}

void ModuleSequence::FadeIn() {
	JD8_Blit(fondo);
	JG_SetUpdateTicks(1);
	while (!JD8_FadeToPalAsync(pal) && !JG_Quitting()) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
	}
}

void ModuleSequence::FadeOut() {
	JG_SetUpdateTicks(2);
	while (!JD8_FadeOutAsync() && !JG_Quitting()) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
	}
}

void ModuleSequence::ShowText(int x, int y, int color, int speed, const char * text1, const char * text2, const char * text3) {
	bool eixir = false;

	JG_SetUpdateTicks(10);
	JD8_SetPaletteColor(254, color);

	// mod 35
	// A (65) = 23 -> char - 42
	// Special chars:	( ) � ' - : ! .
	//					[ \ ] ^ _ ` a b 

	int tamany_text1 = strlen(text1);
	int temps_total = tamany_text1 * speed;
	int temps_inicial = JG_GetCycleCounter();

	while ((JG_GetCycleCounter() - temps_inicial < temps_total) && !eixir && !JG_Quitting()) {
		JD8_Blit(fondo);
		int num_chars_actual = (JG_GetCycleCounter() - temps_inicial) / speed;

		for (int i = 0; i <= num_chars_actual; i++) {
			int char_actual = text1[i] - 42; if (char_actual < 0) char_actual = 57;
			if (text1[i] == '.') char_actual = 56;
			JD8_BlitCK(x + (i+1) * 8, y, gfx, (char_actual % 35) * 9, 6+floorf(char_actual / 35) * 9, 9, 9, 255);
		}
		while (!JG_ShouldUpdate()) { JI_Update(); if (JI_AnyKey()) { eixir = true; } }
		JD8_Flip();
	}
	if (eixir) {
		for (int i = 0; i <= tamany_text1; i++) {
			int char_actual = text1[i] - 42; if (char_actual < 0) char_actual = 57;
			if (text1[i] == '.') char_actual = 56;
			JD8_BlitCK(x + (i + 1) * 8, y, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
		}
	}

	if (text2 != NULL) {
		int tamany_text2 = strlen(text2);
		temps_total = tamany_text2 * speed;
		temps_inicial = JG_GetCycleCounter();

		while ((JG_GetCycleCounter() - temps_inicial < temps_total) && !eixir && !JG_Quitting()) {
			JD8_Blit(fondo);
			int num_chars_actual = (JG_GetCycleCounter() - temps_inicial) / speed;

			for (int i = 0; i <= tamany_text1; i++) {
				int char_actual = text1[i] - 42; if (char_actual < 0) char_actual = 57;
				if (text1[i] == '.') char_actual = 56;
				JD8_BlitCK(x + (i + 1) * 8, y, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
			}

			for (int i = 0; i <= num_chars_actual; i++) {
				int char_actual = text2[i] - 42; if (char_actual < 0) char_actual = 57;
				if (text2[i] == '.') char_actual = 56;
				JD8_BlitCK(x + (i + 1) * 8, y+10, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
			}
			while (!JG_ShouldUpdate()) { JI_Update(); if (JI_AnyKey()) { eixir = true; } }
			JD8_Flip();
		}

		if (eixir) {
			for (int i = 0; i <= tamany_text2; i++) {
				int char_actual = text2[i] - 42; if (char_actual < 0) char_actual = 57;
				if (text2[i] == '.') char_actual = 56;
				JD8_BlitCK(x + (i + 1) * 8, y + 10, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
			}
		}

		if (text3 != NULL) {
			int tamany_text3 = strlen(text3);
			temps_total = tamany_text3 * speed;
			temps_inicial = JG_GetCycleCounter();

			while ((JG_GetCycleCounter() - temps_inicial < temps_total) && !eixir && !JG_Quitting()) {
				JD8_Blit(fondo);
				int num_chars_actual = (JG_GetCycleCounter() - temps_inicial) / speed;

				for (int i = 0; i <= tamany_text1; i++) {
					int char_actual = text1[i] - 42; if (char_actual < 0) char_actual = 57;
					if (text1[i] == '.') char_actual = 56;
					JD8_BlitCK(x + (i + 1) * 8, y, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
				}

				for (int i = 0; i <= tamany_text2; i++) {
					int char_actual = text2[i] - 42; if (char_actual < 0) char_actual = 57;
					if (text2[i] == '.') char_actual = 56;
					JD8_BlitCK(x + (i + 1) * 8, y+10, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
				}

				for (int i = 0; i <= num_chars_actual; i++) {
					int char_actual = text3[i] - 42; if (char_actual < 0) char_actual = 57;
					if (text3[i] == '.') char_actual = 56;
					JD8_BlitCK(x + (i + 1) * 8, y + 20, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
				}
				while (!JG_ShouldUpdate()) { JI_Update(); if (JI_AnyKey()) { eixir = true; } }
				JD8_Flip();
			}

			for (int i = 0; i <= tamany_text3; i++) {
				int char_actual = text3[i] - 42; if (char_actual < 0) char_actual = 57;
				if (text3[i] == '.') char_actual = 56;
				JD8_BlitCK(x + (i + 1) * 8, y + 20, gfx, (char_actual % 35) * 9, 6 + floorf(char_actual / 35) * 9, 9, 9, 255);
			}
			JD8_Flip();
		}
	}

	JD8_Flip();
	Wait(200);
}

void ModuleSequence::Wait(int time) {
	bool eixir = false;
	int temps_inicial = JG_GetCycleCounter();
	while ((JG_GetCycleCounter() - temps_inicial < time) && !eixir) {
		while (!JG_ShouldUpdate()) { JI_Update(); if (JI_AnyKey() || JG_Quitting()) { eixir = true; } }
	}
}
