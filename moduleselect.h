#pragma once

#include "info.h"

class ModuleSelect {

public:

	ModuleSelect(Info* info);
	~ModuleSelect(void);

	int Go();

private:

	Info* info;
};
