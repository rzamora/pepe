#include "jfile.h"
#include <SDL2/SDL.h>
#include <fstream>

#pragma pack(push,1)

struct DATA_Header {
	char magic[4];
	Uint32 num_files;
	Uint32 index_offset;
};

struct DATA_Info {
	Uint32	offset;
	Uint32	length;
	char	name[13];
};

struct DATA_Index {
	DATA_Info*	file_info;
};

struct DATA_File {
	DATA_Header	header;
	DATA_Index	index;
};

#pragma pack(pop)

const char *resourceFileName = "data.jrf";
DATA_File *data_file = NULL;

void JF_SetResourceFile(const char *p_resourceFileName) {
	resourceFileName = p_resourceFileName;
}


void JF_GetDataFile() {
	std::ifstream fd( resourceFileName, std::ios::in | std::ios::binary );

	if( fd.fail() ) {
		perror("No s'ha pogut obrir l'arxiu de recursos");
		exit(1);
	}
	
	data_file = (DATA_File*)malloc( sizeof( DATA_File ) );

	fd.read( (char*)&data_file->header, sizeof( DATA_Header ) );

	fd.seekg( data_file->header.index_offset );

	data_file->index.file_info = (DATA_Info*)malloc( data_file->header.num_files * sizeof( DATA_Info ) );

	fd.read( (char*)data_file->index.file_info, data_file->header.num_files * sizeof( DATA_Info ) );

	fd.close();
}

char *JF_GetBufferFromResource(const char *resourcename, int& filesize) {

	if( data_file == NULL ) {
		JF_GetDataFile();
	}

	bool found = false;
	int count = 0;
	while( !found && count < data_file->header.num_files ) {
		found = ( strcmp( resourcename, data_file->index.file_info[count].name ) == 0 );
		if( !found ) count++;
	}

	if( !found ) {
		perror("El recurs no s'ha trobat en l'arxiu de recursos");
		exit(1);
	}

	filesize = data_file->index.file_info[count].length;

	std::ifstream fd( resourceFileName, std::ios::in | std::ios::binary );

	if( fd.fail() ) {
		perror("No s'ha pogut obrir l'arxiu de recursos");
		exit(1);
	}

	fd.seekg( data_file->index.file_info[count].offset );
	
	char* buffer = (char*)malloc( filesize );
	fd.read( buffer, filesize );

	fd.close();

	return buffer;
}

//	//Read the first INT, which will tell us how many files are in this resource
//	int numfiles;
//	int resultat = read(fd, &numfiles, sizeof(int));
//	
//#ifdef _WIN32
//	int final = eof(fd);
//#endif
//
//	//Get the pointers to the stored files
//	int *filestart = (int *) malloc(sizeof(int) * numfiles);
//	resultat = read(fd, filestart, sizeof(int) * numfiles);
//	
//	//Loop through the files, looking for the file in question
//	int filenamesize;
//	char *buffer;
//	int i;
//	for(i=0;i<numfiles;i++) 
//	{
//		int result = 129;
//		char *filename;
//		//Seek to the location
//		lseek(fd, filestart[i], SEEK_SET);
//		//Get the filesize value
//		read(fd, filesize, sizeof(int));
//		//Get the size of the filename string
//		read(fd, &filenamesize, sizeof(int));
//		//Size the buffer and read the filename
//		filename = (char *) malloc(filenamesize + 1);
//		result = read(fd, filename, filenamesize);
//		//Remember to terminate the string properly!
//		filename[filenamesize] = '\0';
//		//Compare to the string we're looking for
//		if (strcmp(filename, resourcename) == 0) 
//		{
//			//Get the contents of the file
//			buffer = (char *) malloc(*filesize);
//			read(fd, buffer, *filesize);
//			free(filename);
//			break;
//		}
//		//Free the filename buffer
//		free(filename);
//	}
//	
//	//Release memory
//	free(filestart);
//	
//	//Close the resource file!
//	close(fd);
//	
//	//Did we find the file within the resource that we were looking for?
//	if (buffer == NULL) 
//	{
//		printf("Unable to find '%s' in the resource file!\n", resourcename);
//		exit(1);
//	}
//	
//	//Return the buffer
//	return buffer;
//}

void JF_Quit() {
	if( data_file != NULL ) {
		free( data_file->index.file_info );
		free( data_file );
	}
}
