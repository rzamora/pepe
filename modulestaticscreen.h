#pragma once

#include "info.h"

class ModuleStaticScreen {

public:

	ModuleStaticScreen( Info* info );
	~ModuleStaticScreen(void);

	int Go();

private:

	void doIcekas();
	void doLogo();

	Info* info;
	int contador;
};
