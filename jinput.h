#pragma once
#include <SDL2/SDL.h>

	void JI_DisableKeyboard(Uint32 time);

	void JI_Update();

	bool JI_KeyPressed(int key);

	bool JI_CheatActivated( const char* cheat_code );

	bool JI_AnyKey();
