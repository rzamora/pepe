#include "modulemenu.h"

#include "jgame.h"
#include "jdraw8.h"
#include "jinput.h"
#include "jsound.h"
#include <stdlib.h>
#include <string>

#define SENSE_OPCIO		 -1
#define OPCIO_TIMEATTACK  0
#define OPCIO_WORLDTOUR   1
#define OPCIO_CLASSICMODE 2
#define OPCIO_OPTIONS     3

ModuleMenu::ModuleMenu(Info* info) {
	this->info = info;
}

ModuleMenu::~ModuleMenu(void) {
}

int ModuleMenu::Go() {

	JS_LoadMusic("mtitle.mid");
	JS_PlayMusic(-1);

	JD8_Surface fondo = JD8_LoadSurface("menu.gif");
	JD8_Palette pal = JD8_LoadPalette("menu.gif");
	JD8_Surface opcions = JD8_LoadSurface("menu_s.gif");
	JD8_Surface gfx = JD8_LoadSurface("sprites.gif");

	JD8_ClearScreen(0);
	JD8_Flip();

	// FADE IN
	JD8_Blit(fondo);
	JG_SetUpdateTicks(1);
	while (!JD8_FadeToPalAsync(pal) && !JG_Quitting()) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
	}

	int x = 320;
	while (!JG_Quitting() && x > 0) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
		JD8_Blit(fondo);
		JD8_BlitCKCut(x, 50, opcions, 0, 50, 320, 38, 0);
		JD8_Flip();
		x--;
	}
	JD8_BlitCKToSurface(0, 50, opcions, 0, 50, 320, 38, fondo, 0);

	x = 320;
	while (!JG_Quitting() && x > 25) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
		JD8_Blit(fondo);
		JD8_BlitCKCut(x, 88, opcions, 25, 88, 135, 37, 0);
		JD8_Flip();
		x--;
	}
	JD8_BlitCKToSurface(25, 88, opcions, 25, 88, 135, 37, fondo, 0);

	x = 320;
	while (!JG_Quitting() && x > 0) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
		JD8_Blit(fondo);
		JD8_BlitCKCut(x, 125, opcions, 0, 125, 320, 38, 0);
		JD8_Flip();
		x--;
	}
	JD8_BlitCKToSurface(0, 125, opcions, 0, 125, 320, 38, fondo, 0);

	x = 320;
	while (!JG_Quitting() && x > 0) {
		while (!JG_ShouldUpdate()) { JI_Update();  }
		JD8_Blit(fondo);
		JD8_BlitCKCut(x, 163, opcions, 0, 163, 320, 37, 0);
		JD8_Flip();
		x--;
	}
	JD8_BlitCKToSurface(0, 163, opcions, 0, 163, 320, 37, fondo, 0);

	int y = 50;
	while (!JG_Quitting() && y > 0) {
		while (!JG_ShouldUpdate()) { JI_Update(); }
		JD8_Blit(fondo);
		JD8_BlitCKCut(0, -y, opcions, 0, 0, 320, 50, 0);
		JD8_Flip();
		y--;
	}
	JD8_BlitCKToSurface(0, 0, opcions, 0, 0, 320, 50, fondo, 0);

	int opcio_seleccionada = OPCIO_TIMEATTACK;
	int opcio_triada = SENSE_OPCIO;
	bool fletxa_amunt_pulsada = false;
	bool fletxa_avall_pulsada = false;
	float x_fletxa = 0;
	float x_velocitat = 1;

	JG_SetUpdateTicks(20);

	while (!JG_Quitting() && opcio_triada == SENSE_OPCIO) {
		while (!JG_ShouldUpdate()) {
			JI_Update();
			if (JI_KeyPressed(SDL_SCANCODE_UP)) {
				if (!fletxa_amunt_pulsada) {
					fletxa_amunt_pulsada = true;
					opcio_seleccionada--; if (opcio_seleccionada < 0) opcio_seleccionada = 3;
				}
			} else {
				fletxa_amunt_pulsada = false;
			}
			if (JI_KeyPressed(SDL_SCANCODE_DOWN)) {
				if (!fletxa_avall_pulsada) {
					fletxa_avall_pulsada = true;
					opcio_seleccionada++; if (opcio_seleccionada > 3) opcio_seleccionada = 0;
				}
			} else {
				fletxa_avall_pulsada = false;
			}
			if (JI_KeyPressed(SDL_SCANCODE_SPACE)) {
					opcio_triada = opcio_seleccionada;
			}
		}

		x_fletxa += x_velocitat;
		x_velocitat -= 0.1;

		if (x_fletxa < 0) {
			x_fletxa = 0;
			x_velocitat = 1;
		}
		JD8_Blit(fondo);
		JD8_BlitCK(10+x_fletxa, 60+opcio_seleccionada*37, gfx, 18, 5, 9, 10, 255);
		JD8_Flip();
	}
	JD8_BlitCKToSurface(10 + x_fletxa, 60 + opcio_seleccionada * 37, gfx, 18, 5, 9, 10, fondo, 255);

	// BLINDS OUT
	JG_SetUpdateTicks(8);
	x = 0;
	while (x < 32 && !JG_Quitting()) {
		while (!JG_ShouldUpdate()) { JI_Update(); }

		for (y = 0; y < 200; y++) {
			for (int i = 0; i < 10; i++) {
				JD8_PutPixel(fondo, i * 32 + x, y, 0);
			}
		}
		x++;
		JD8_Blit(fondo);
		JD8_Flip();
	}

	// FREE EVERYTHING
	JD8_FreeSurface(fondo);
	JD8_FreeSurface(opcions);
	JD8_FreeSurface(gfx);
	free(pal);




	if (JG_Quitting()) {
		return ESTAT_EIXIR;
	} else {
		switch (opcio_triada) {
		case OPCIO_TIMEATTACK:	return ESTAT_LOGO; break;
		case OPCIO_WORLDTOUR:	return ESTAT_SELECT; break;
		case OPCIO_CLASSICMODE: return ESTAT_MENU; break;
		case OPCIO_OPTIONS:		return ESTAT_MENU; break;
		}
	}
	return ESTAT_EIXIR;
}
