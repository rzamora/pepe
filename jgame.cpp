#include "jgame.h"

bool eixir = false;
int updateTicks = 0;
Uint32 updateTime = 0;
Uint32 cycle_counter = 0;

void JG_Init() {

	SDL_Init( SDL_INIT_EVERYTHING );
	//SDL_WM_SetCaption( title, NULL );
	updateTime = SDL_GetTicks();
}

void JG_Finalize() {
	SDL_Quit();
}

void JG_QuitSignal() {
	eixir = true;
}

bool JG_Quitting() {
	return eixir;
}

void JG_SetUpdateTicks(Uint32 milliseconds) {
	updateTicks = milliseconds;
}

bool JG_ShouldUpdate() {
	if (SDL_GetTicks() - updateTime > updateTicks) {
		updateTime = SDL_GetTicks();
		cycle_counter++;
		return true;
	} else {
		return false;
	}
}

Uint32 JG_GetCycleCounter() {
	return cycle_counter;
}
