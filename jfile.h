#pragma once

void JF_SetResourceFile(const char *p_resourceFileName);

char *JF_GetBufferFromResource(const char *resourcename, int& filesize);

void JF_Quit();