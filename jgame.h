#pragma once
#include <SDL2/SDL.h>
 
void JG_Init();

void JG_Finalize();

void JG_QuitSignal();

bool JG_Quitting();

void JG_SetUpdateTicks(Uint32 milliseconds);

bool JG_ShouldUpdate();

Uint32 JG_GetCycleCounter();
