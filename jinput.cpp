#include "jinput.h"
#include "jgame.h"
#include <string>

const Uint8 *keystates;// = SDL_GetKeyboardState( NULL );
SDL_Event event;
Uint8 cheat[5];
bool key_pressed = false;
int waitTime = 0;

void JI_DisableKeyboard(Uint32 time) {
	waitTime = time;
}

void JI_moveCheats( Uint8 new_key ) {
	cheat[0] = cheat[1];
	cheat[1] = cheat[2];
	cheat[2] = cheat[3];
	cheat[3] = cheat[4];
	cheat[4] = new_key;
}

void JI_Update() {
	key_pressed = false;
	keystates = SDL_GetKeyboardState( NULL );	

	if (waitTime > 0) waitTime--;

	while ( SDL_PollEvent( &event ) ) {
		if ( event.type == SDL_QUIT ) JG_QuitSignal();
		if( event.type == SDL_KEYUP ) {
			key_pressed = true;
			JI_moveCheats( event.key.keysym.sym );
		}
	}
}

bool JI_KeyPressed(int key) {
	return waitTime > 0 ? false : (keystates[key] != 0);
}

bool JI_CheatActivated( const char* cheat_code ) {
	bool found = true;
	for( int i = 0; i < strlen( cheat_code ); i++ ) {
		if( cheat[i] != cheat_code[i] ) found = false;
	}
	return found;
}

bool JI_AnyKey() {
	return waitTime > 0 ? false : key_pressed;
}
